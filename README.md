## Requirements
- Composer and NPM installed
- An Authy API Key
## Set up
Add your Authy API key to the `.env` file as in below
```
AUTHY_SECRET=<YOUR AUTHY SECRET HERE>
```
Install composer and npm dependencies.
```bash
composer install
npm install
npm run dev
```
Run your database migrations and seeds with the command below:
```
php artisan migrate
php artisan db:seed
```
Start the PHP server with `php artisan serve`.

Head over to <http://localhost:8000/auth/register/> to create a new user.
On logging in, you should see all notes whose `user_id` is same as the
logged-in user. Try deleting any of the notes and you should see the middleware
in action.

