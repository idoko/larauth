<?php

namespace App\Http\Controllers\Auth;

use Authy\AuthyApi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;

class AuthyController
{
    public function showVerifyForm(Request $request)
    {
        return view('auth.2fa');
    }

    public function verify(Request $request)
    {
        $request->validate([
            'token' => ['required', 'numeric', 'digits_between:6,10'],
        ]);

        $authy = new AuthyApi(getenv("AUTHY_SECRET"));
        $verification = $authy->verifyToken(auth()->user()->authy_id, $request->input("token"));

        try {
            if ($verification->ok()) {
                session()->put("is_verified", true);
                session()->put("verified_at", Carbon::now()->timestamp);
                return redirect()->intended();
            } else {
                Log::info(json_encode($verification->errors()));
                $errors = new MessageBag(['token' => ['Failed to verify token']]);
                return back()->withErrors($errors);
            }
        } catch (\Throwable $t) {
            Log::error(json_encode($t));
            $errors = new MessageBag(['token' => [$t->getMessage()]]);
            return back()->withErrors($errors);
        }
    }
}
