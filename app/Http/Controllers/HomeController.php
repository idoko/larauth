<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $notes = Note::where('user_id', auth()->user()->id)
            ->orderBy('id', "desc")
            ->get();
        $data = [
            'notes' => $notes
        ];
        return view('home', $data);
    }

    public function viewNote($noteId) {
        $note = Note::find($noteId);
        if ($note->user_id != auth()->id()) {
            abort(404);
        }
        return view('note', ['note' => $note]);
    }

    public function deleteNote(Request $request) {
        $note = Note::findOrFail($request->input('note_id'));
        if (auth()->user()->id != $note->user_id) {
            abort(404);
        }
        $note->delete();
        return redirect('/notes');
    }
}
