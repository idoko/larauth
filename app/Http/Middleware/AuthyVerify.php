<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class AuthyVerify
{
    // How long to let the elevated authentication last
    const VERIFICATION_TIMEOUT = 10;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->needsVerification()) {
            return redirect('/auth/verify');
        }
        return $next($request);
    }

    public function needsVerification() {
        $verifiedAt = Carbon::createFromTimestamp(session('verified_at', 0));
        $timePast = $verifiedAt->diffInMinutes();

        return (!session()->get("is_verified", false)) ||
            ($timePast > self::VERIFICATION_TIMEOUT);
    }
}
