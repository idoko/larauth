<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Note::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(10, true),
        'body' => $faker->text(250),
        'user_id' => rand(1, 3)
    ];
});
