@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Enter your Authy Code') }}</div>
                    <div class="card-body">

                        <p>
                            {{ __('You are about to perform a sensitive operation, please verify your identity to continue.') }}
                        </p>
                        <form method="POST" action="{{ route('authy.verify') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="token" class="col-md-2 col-form-label text-md-left">{{ __('Token') }}</label>
                                <div class="col-md-6">
                                    <input id="token" type="text" class="form-control @error('token') is-invalid @enderror" name="token" value="{{ old('token') }}" autofocus>
                                    @error('token')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary align-baseline">{{ __('Verify') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
