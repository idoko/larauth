@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach ($notes as $note)
                    <div class="row">
                        <div class="col-md-12">
                            <h2><a href="{{route('note.view', $note->id)}}">{{$note->title}}</a></h2>
                            <div>
                                <span class="float-left">{{$note->created_at}}</span>
                                <div class="float-right">
                                    <form action="{{route('note.delete')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="note_id" value="{{$note->id}}" />
                                        <button type="submit" href="{{route('note.delete', $note->id)}}"
                                                class="btn btn-sm btn-outline-dark m-2">Delete
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    </div>
    </div>
@endsection
