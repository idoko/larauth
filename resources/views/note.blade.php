@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>{{$note->title}}</h2>
                <div>
                    {{$note->body}}
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
