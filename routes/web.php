<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'auth'], function() {
   Auth::routes();
   Route::get('verify', 'Auth\AuthyController@showVerifyForm')->name('authy.show-form');
   Route::post('verify', 'Auth\AuthyController@verify')->name('authy.verify');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/notes', 'HomeController@index');
Route::get('/note/{noteId}', 'HomeController@viewNote')->name('note.view');
Route::post('/notes/delete', 'HomeController@deleteNote')
    ->name('note.delete')
    ->middleware('authy.verify');
